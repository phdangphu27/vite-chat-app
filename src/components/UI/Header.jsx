import { Link } from "react-router-dom";

const Header = () => {
  return (
    <div className='flex justify-between text-white bg-black p-3'>
      <Link to={"/"}>LOGO</Link>
      <button className='bg-red-500 p-3 rounded'>Sign out</button>
    </div>
  );
};

export default Header;
