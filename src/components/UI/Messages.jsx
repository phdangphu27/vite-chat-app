import React from "react";
import Message from "./Message";

const Messages = () => {
  return (
    <div
      className='bg-blue-50 p-3 flex-1 overflow-y-scroll'
      style={{ maxHeight: "600px" }}
    >
      <Message />
      <Message />
      <Message />
      <Message />
      <Message />
      <Message />
      <Message />
      <Message />
      <Message />
      <Message />
      <Message />
    </div>
  );
};

export default Messages;
