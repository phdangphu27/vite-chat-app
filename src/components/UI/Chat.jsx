import Messages from "./Messages";
import Input from "./Input";

const Chat = () => {
  return (
    <div className='col-span-2 text-white flex flex-col'>
      <div className='flex bg-blue-900 p-3'>
        <span>THE ROCK</span>
      </div>
      <Messages />
      <Input />
    </div>
  );
};

export default Chat;
