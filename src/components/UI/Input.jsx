const Input = () => {
  return (
    <div className='bg-white p-3 text-black flex justify-between'>
      <input
        className='focus:outline-none border-none p-3 w-full'
        placeholder='Type something...'
        type='text'
      />
      <div className='flex items-center'>
        <input type='file' />
        <button className='border-none text-white bg-blue-900 p-2 rounded'>
          Send
        </button>
      </div>
    </div>
  );
};

export default Input;
