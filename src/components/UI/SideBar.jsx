import Channel from "./Channel";

const SideBar = () => {
  return (
    <div className='bg-blue-900 flex flex-col'>
      <div className='p-3 bg-blue-800'>
        <div className='flex justify-between'>
          <h1 className='text-2xl font-bold text-white'>THE CHAT APP</h1>
          <div className='flex items-center'>
            <img src='adsdas' />
            <span className='text-white'>JOHN CENA</span>
            <button className='p-2 bg-blue-900 rounded text-white'>
              logout
            </button>
          </div>
        </div>
      </div>
      <input
        placeholder='search for conversation'
        className='text-white focus:outline-none p-2 bg-transparent border-b-2 border-white'
      />
      <Channel />
      <Channel />
      <Channel />
      <Channel />
      <Channel />
      <Channel />
      <Channel />
    </div>
  );
};

export default SideBar;
