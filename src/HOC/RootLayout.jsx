import { Outlet } from "react-router-dom";

const RootLayout = () => {
  return (
    <div className='bg-blue-100'>
      <Outlet />
    </div>
  );
};

export default RootLayout;
