import "./App.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import SignIn from "./pages/SignIn";
import Chats from "./pages/Chats";
import RootLayout from "./HOC/RootLayout";
import Error from "./pages/Error";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    errorElement: <Error />,
    children: [
      { path: "/", element: <SignIn /> },
      { path: "/chats", element: <Chats /> },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
