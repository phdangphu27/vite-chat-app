// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDCNQktO6w7Ot6bvU0SK70PyVjPqBRnYv0",
  authDomain: "vite-chat-cf735.firebaseapp.com",
  projectId: "vite-chat-cf735",
  storageBucket: "vite-chat-cf735.appspot.com",
  messagingSenderId: "250573157783",
  appId: "1:250573157783:web:c95843863a2ad870dcc0d2",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);
