import { auth } from "../firebase";
import { useEffect, useState } from "react";
import { FcGoogle } from "react-icons/fc";

const SignIn = () => {
  const [isSignIn, setIsSignIn] = useState(true);

  return (
    <div className='mx-auto min-h-screen' style={{ maxWidth: "450px" }}>
      {isSignIn ? (
        <form className='flex flex-col min-w-fit bg-blue-200 p-5 rounded'>
          <div className='text-center'>
            <h1 className='text-2xl font-bold text-blue-700'>THE CHAT APP</h1>
            <p className='font-bold'>Sign In</p>
          </div>
          <input
            className='my-3 p-3 rounded border-blue-700 border-b-2 focus:outline-none'
            placeholder='email'
          />
          <input
            className='my-3 p-3 rounded border-blue-700 border-b-2 focus:outline-none'
            placeholder='password'
          />
          <button className='p-3 bg-blue-700 rounded text-white'>
            Sign In
          </button>
          <h2 className='text-center p-2'>or continue with</h2>
          <button className='flex items-center justify-center p-3 rounded  text-white bg-blue-700'>
            <FcGoogle className='text-3xl mr-3' />
            Google
          </button>
          <div className='flex p-2'>
            <p>{`Don't have an account?`}</p>
            <button
              className='pl-3 text-blue-800'
              type='button'
              onClick={() => {
                setIsSignIn(false);
              }}
            >
              Sign Up
            </button>
          </div>
        </form>
      ) : (
        <form className='flex flex-col min-w-fit bg-blue-200 p-5 rounded'>
          <div className='text-center'>
            <h1 className='text-2xl font-bold text-blue-700'>THE CHAT APP</h1>
            <p className='font-bold'>Sign Up</p>
          </div>
          <input
            className='my-3 p-3 rounded border-blue-800 border-b-2 focus:outline-none'
            placeholder='display name'
          />
          <input
            className='my-3 p-3 rounded border-blue-800 border-b-2 focus:outline-none'
            placeholder='email'
          />
          <input
            className='my-3 p-3 rounded border-blue-800 border-b-2 focus:outline-none'
            placeholder='password'
          />
          <input className='p-3 focus:outline-none' type='file' />
          <button className='p-3 bg-blue-800 rounded text-white'>
            Sign Up
          </button>
          <div className='flex p-2'>
            <p>{`Already have an account?`}</p>
            <button
              className='pl-3 text-blue-800 '
              type='button'
              onClick={() => {
                setIsSignIn(true);
              }}
            >
              Sign In
            </button>
          </div>
        </form>
      )}
    </div>
  );
};

export default SignIn;
