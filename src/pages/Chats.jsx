import SideBar from "../components/UI/SideBar";
import Chat from "../components/UI/Chat";

const Chats = () => {
  return (
    <div className='min-h-screen'>
      <div className='grid grid-cols-3 border-2 border-white w-3/5'>
        <SideBar />
        <Chat />
      </div>
    </div>
  );
};

export default Chats;
