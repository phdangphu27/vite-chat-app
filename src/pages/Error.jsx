import { Outlet } from "react-router-dom";
import Header from "../components/UI/Header";
import Footer from "../components/UI/Footer";

const Error = () => {
  return (
    <div>
      <Header />
      <h1>404 NOT FOUND !</h1>
      <p>PLEASE CHECK YOUR URL</p>
      <Outlet />
      <Footer />
    </div>
  );
};

export default Error;
